import { useState } from "react"
import "./Counter.css"

const Counter = () => {
  const [counter, setCounter] = useState(0)
  const [number, setNumber] = useState(1)

  const minus = () => setCounter(counter - number)
  const plus = () => setCounter(number + counter)

  return (
    <div>
      <h3 data-testid="title">Counter Apps</h3>
      <h1
        className={`${counter >= 100 ? "green" : ""} ${counter <= -100 ? "red": ""}`}
        data-testid="counter"
      >{counter}</h1>
      <button data-testid="minusBtn" onClick={minus}>-</button>
      <input
        data-testid="number"
        className="text-center"
        type="number" 
        value={number} 
        onChange={(e) => setNumber(parseInt(e.target.value))}
      />
      <button data-testid="plusBtn" onClick={plus}>+</button>
    </div>
  )
}

export default Counter
