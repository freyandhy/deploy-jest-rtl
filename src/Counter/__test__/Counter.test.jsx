import Counter from "../Counter"
import { render, fireEvent } from "@testing-library/react"

test("Ada titlenya", () => {
  // const component = render(<Counter />)
  // const titleEl = component.getByTestId("title")
  const { getByTestId } = render(<Counter />)
  const titleEl = getByTestId("title")


  expect(titleEl.textContent).toBe("Counter Apps")
})

test("Nilai awal counternya adalah 0", () => {
  const { getByTestId } = render(<Counter />)
  const counterEl = getByTestId("counter")

  expect(counterEl.textContent).toBe("0")
})

test("Nilai awal inputnya adalah 1", () => {
  const { getByTestId } = render(<Counter />)
  const inputEl = getByTestId("number")

  expect(inputEl.value).toBe("1")
})

test("Ada button plus", () => {
  const { getByTestId } = render(<Counter />)
  const plusBtn = getByTestId("plusBtn")

  expect(plusBtn.textContent).toBe("+")
})

test("Ada button minus", () => {
  const { getByTestId } = render(<Counter />)
  const minusBtn = getByTestId("minusBtn")

  expect(minusBtn.textContent).toBe("-")
})

test("Pastikan number berubah", () => {
  const { getByTestId } = render(<Counter />)
  const inputEl = getByTestId("number")

  fireEvent.change(inputEl, {
    target: {
      value: "5"
    }
  })

  expect(inputEl.value).toBe("5")
})